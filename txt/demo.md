一 雪花算法
    1.数据库的扩展主要包括:业务分库,主从复制,数据库分表
    2.长度64位,41bit为时间戳,10bit为机器ID,12bit作为毫内的流水号
    3,优点:整体上按照时间自增排序,并且基本上不会发生相同ID碰撞
二 yml配置问题
    1.spring:
#配置数据源信息
datasource:
#配置数据源类型
type: com.zaxxer.hikari.HikariDataSource
#配置链接数据各个信息
driver-class-name: com.mysql.cj.jdbc.Driver
url: jdbc:mysql://localhost:3306/mybatis_plus?serverTimezone=GMT%2B8&characterEncoding=utf-8&useSSL=false&allowPublicKeyRetrieval=true
username: root
password: root
server:
port: 8080
#配置日志,控制台输出mybatis-plus提供的sql
mybatis-plus:
configuration:
log-impl: org.apache.ibatis.logging.stdout.StdOutImpl
# 设置mybatis-plus的全局配置
global-config:
db-config:
#设置表的统一前缀
table-prefix: t_
#设置统一的主键生成策略
id-type: auto

2.每天都会连接JDBC失败,终端启动执行MySQL之后就没有问题了,url后面的allowPublicKeyRetrieval=true是后加上的,解决SQLNonTransientConnectionException: Public Key Retrieval is not allowed
    问题
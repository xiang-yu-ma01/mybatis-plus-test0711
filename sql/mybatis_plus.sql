-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        8.0.29 - MySQL Community Server - GPL
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- 导出 mybatis_plus 的数据库结构
CREATE DATABASE IF NOT EXISTS `mybatis_plus` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `mybatis_plus`;

-- 导出  表 mybatis_plus.t_product 结构
CREATE TABLE IF NOT EXISTS `t_product` (
  `id` bigint NOT NULL COMMENT '主键ID',
  `NAME` varchar(30) DEFAULT NULL COMMENT '商品名称',
  `price` int DEFAULT '0' COMMENT '价格',
  `VERSION` int DEFAULT '0' COMMENT '乐观锁版本号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 正在导出表  mybatis_plus.t_product 的数据：~1 rows (大约)
DELETE FROM `t_product`;
/*!40000 ALTER TABLE `t_product` DISABLE KEYS */;
INSERT INTO `t_product` (`id`, `NAME`, `price`, `VERSION`) VALUES
	(1, '惠普游戏本', 120, 5);
/*!40000 ALTER TABLE `t_product` ENABLE KEYS */;

-- 导出  表 mybatis_plus.t_user 结构
CREATE TABLE IF NOT EXISTS `t_user` (
  `uid` int NOT NULL AUTO_INCREMENT,
  `user_name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8_general_ci DEFAULT NULL,
  `age` int DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `is_deleted` int NOT NULL DEFAULT '0',
  `sex` int DEFAULT NULL,
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3;

-- 正在导出表  mybatis_plus.t_user 的数据：~7 rows (大约)
DELETE FROM `t_user`;
/*!40000 ALTER TABLE `t_user` DISABLE KEYS */;
INSERT INTO `t_user` (`uid`, `user_name`, `age`, `email`, `is_deleted`, `sex`) VALUES
	(1, 'Jone', 18, 'test1@maxiangyu.com', 1, NULL),
	(2, 'Jack', 19, 'test2@maxiangyu.com', 1, NULL),
	(3, 'Tom', 23, 'test3@maxiangyu.com', 1, NULL),
	(4, '胡歌', 26, 'maxiangyu@qq.com', 0, NULL),
	(5, '胡歌', 17, 'maxiangyu@qq.com', 0, NULL),
	(6, '胡歌', 24, 'maxiangyu@qq.com', 0, NULL),
	(7, 'admin', NULL, NULL, 1, NULL),
	(8, 'admin', 20, NULL, 0, 1);
/*!40000 ALTER TABLE `t_user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;

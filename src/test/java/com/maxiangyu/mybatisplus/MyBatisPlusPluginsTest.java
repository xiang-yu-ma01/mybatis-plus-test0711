package com.maxiangyu.mybatisplus;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.maxiangyu.mybatisplus.mapper.ProductMapper;
import com.maxiangyu.mybatisplus.mapper.UserMapper;
import com.maxiangyu.mybatisplus.pojo.Product;
import com.maxiangyu.mybatisplus.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @ClassName: MyBatisPlusPluginsTest
 * @Description:
 * @Author: maxiangyu
 * @Date: 2022/07/15 11:40
 * @Version 1.0
 */
@SpringBootTest
public class MyBatisPlusPluginsTest {
    @Autowired
    private UserMapper usermapper;

    @Autowired
    private ProductMapper productMapper;

    @Test
    public void testPage(){
        //current:访问第一页,size=2:每页访问2条数据
        Page<User> page = new Page<>(1,2);
        //SELECT uid AS id,user_name AS name,age,email,is_deleted FROM t_user WHERE is_deleted=0 LIMIT ?
        usermapper.selectPage(page,null);
        //page:com.baomidou.mybatisplus.extension.plugins.pagination.Page@52285a5f
        System.out.println("page:"+page.getRecords());
        System.out.println(page.getCurrent());//获取页码
        System.out.println(page.getPages());//获取总页数
        System.out.println(page.getTotal());
        System.out.println(page.hasNext());
        System.out.println(page.hasPrevious());
    }

    @Test
    public void testPageVo(){
        Page<User> page = new Page<>(1,3);
        usermapper.selectPageVo(page,20);
        System.out.println("page:"+page.getRecords());
        System.out.println(page.getCurrent());//获取页码
        System.out.println(page.getPages());//获取总页数
        System.out.println(page.getTotal());
        System.out.println(page.hasNext());
        System.out.println(page.hasPrevious());
    }

    @Test
    public void testProduct01(){
        //
        Product productLi = productMapper.selectById(1);
        System.out.println("小李查询的商品价格:"+productLi.getPrice());

        //小王查询商品价格
        Product productWang = productMapper.selectById(1);
        System.out.println("productWang小王查询的商品价格:"+productWang.getPrice());

       //小李将商品价格+50
        productLi.setPrice(productLi.getPrice()+50);
        productMapper.updateById(productLi);

        //小王将商品价格降30
        productWang.setPrice(productWang.getPrice()-30);
        int result = productMapper.updateById(productWang);
        if(result==0){
            //操作失败
            Product productNew = productMapper.selectById(1);
            productNew.setPrice(productNew.getPrice()-30);
            productMapper.updateById(productNew);

        }

        //老板查看商品价格
        Product productBoss = productMapper.selectById(1);
        System.out.println("老板查看商品价格:"+productBoss.getPrice());
    }

}


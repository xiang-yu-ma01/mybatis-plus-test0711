package com.maxiangyu.mybatisplus;


import com.maxiangyu.mybatisplus.mybatisservice.UserService;
import com.maxiangyu.mybatisplus.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: MyBatisPlusServiceTest
 * @Description: MybatisPlusServiece的测试类
 * @Author: maxiangyu
 * @Date: 2022/07/13 09:49
 * @Version 1.0
 */
@SpringBootTest
public class MyBatisPlusServiceTest {
    @Autowired
    private UserService userService;

    @Test
    public void testGetCount(){
        //查询总记录数
        //select count(*) from user
       long count = userService.count();
        System.out.println("总记录数:"+count);
    }
    @Test
    public void testInsertMore(){
        //批量添加
        //insert into user(id,name,age) values(?,?,?)
        List<User> list = new ArrayList<>();
        for (int i = 0;i<11;i++){
            User user = new User();
            user.setId(i+10);
            user.setName("克隆人"+i);
            user.setAge(20+i);
            list.add(user);
        }
        boolean b = userService.saveBatch(list);
        System.out.println(b);
    }

}
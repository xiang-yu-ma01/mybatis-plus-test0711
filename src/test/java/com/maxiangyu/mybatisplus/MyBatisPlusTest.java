package com.maxiangyu.mybatisplus;

import com.maxiangyu.mybatisplus.mapper.UserMapper;
import com.maxiangyu.mybatisplus.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: MyBatisPlusTest
 * @Description: mybatisplus测试类
 * @Author: maxiangyu
 * @Date: 2022/07/11 22:48
 * @Version 1.0
 */
@SpringBootTest
public class MyBatisPlusTest {
    @Autowired
    private UserMapper userMapper;

    @Test
    public void testSelect(){
        //通过条件构造器查询一个list集合,若没有条件,则可以设置为null参数
        List<User> users = userMapper.selectList(null);
        System.out.println(users);
        users.forEach(System.out::println);
    }

    @Test
    public void testInsert(){
        //P14
        //实现新增用户信息
        //INSERT INTO user ( id, name, age, email ) VALUES ( ?, ?, ?, ? )
       User user = new User();
       user.setName("马翔宇");
       user.setAge(24);
       user.setEmail("1506056754@qq.com");
        int result = userMapper.insert(user);
        System.out.println("受影响的行数:"+result);

    }

    @Test
    public void testDelete(){
//        //按id删除数据
//        //sql:DELETE FROM user WHERE id=?
//        int result = userMapper.deleteById(-1254117375);
//        System.out.println("result:"+result);

//        //通过map集合中设置的条件删除用户记录
//        //sql: DELETE FROM user WHERE name = ? AND age = ?
//        Map<String,Object> map = new HashMap<>();
//        map.put("name","马翔宇");
//        map.put("age",24);
//        int result = userMapper.deleteByMap(map);
//        System.out.println("result:"+result);

        //根据id in(),批量删除
        //sql: DELETE FROM user WHERE id IN ( ? , ? , ? )
        List<Integer> list = Arrays.asList(1, 2, 3);
        int result = userMapper.deleteBatchIds(list);
        System.out.println("result"+result);


    }

    @Test
    public void testUpdate(){
        //根据id修改用户数据
        //sql: UPDATE user SET name=?, age=? WHERE id=?
        User user = new User();
        user.setId(4);
        user.setName("胡歌");
        user.setAge(24);
        int sesult = userMapper.updateById(user);
        System.out.println("sesult"+sesult);
    }

    @Test
    public void testSelectT(){
//        //通过id 查询用户信息
//        //slq: SELECT ID,NAME,AGE,EMAIL FROM USER WHERE ID=?
//        User user = userMapper.selectById(4);
//        System.out.println("user:"+user);
//
//        //使用 where id in(?,?,?)形式查询
//        List<Integer> list = Arrays.asList(4, 5, 7);
//        List<User> users = userMapper.selectBatchIds(list);
//        users.forEach(System.out::println);
//
//        //map条件查询
//        //根据map中的集合条件查询
//        Map<String,Object> map = new HashMap<>();
//        map.put("name","胡歌");
//        map.put("age",24);
//        userMapper.selectByMap(map);

//        //查询所有记录数据
//        //select id,name age,email from user;
//        List<User> userList = userMapper.selectList(null);
//        userList.forEach(System.out::println);

        Map<String, Object> map1 = userMapper.selectMapById(4);
        System.out.println("测试输出:"+map1);

    }








}
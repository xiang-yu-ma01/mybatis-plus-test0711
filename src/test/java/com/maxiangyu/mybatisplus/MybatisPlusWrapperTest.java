package com.maxiangyu.mybatisplus;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.maxiangyu.mybatisplus.mapper.UserMapper;
import com.maxiangyu.mybatisplus.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: MybatisPlusWrapperTest
 * @Description:
 * @Author: maxiangyu
 * @Date: 2022/07/14 22:22
 * @Version 1.0
 */
@SpringBootTest
public class MybatisPlusWrapperTest {
    @Autowired
    private UserMapper userMapper;

    @Test
    public void test01(){
        //查询用户名包含a，年龄在20到30之间，邮箱信息部位null的用户信息
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        //SELECT uid AS id,user_name AS name,age,email,is_deleted FROM t_user WHERE is_deleted=0 AND (user_name LIKE ? AND age BETWEEN ? AND ? AND email IS NOT NULL)
        queryWrapper.like("user_name","a").between("age",20,30).isNotNull("email");
        List<User> list = userMapper.selectList(queryWrapper);
        list.forEach(System.out::println);
    }

    @Test
    public void Test02(){
        //查询用户信息,按照年龄降序排序,若年龄相同则id升序排序
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        // SELECT uid AS id,user_name AS name,age,email,is_deleted FROM t_user WHERE is_deleted=0 ORDER BY age DESC,uid ASC
        queryWrapper.orderByDesc("age")
                .orderByAsc("uid");
        List<User> users = userMapper.selectList(queryWrapper);
        users.forEach(System.out::println);


    }

    @Test
    public void Test03(){
        //删除邮箱为null的信息
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();

        //UPDATE t_user SET is_deleted=1 WHERE is_deleted=0 AND (email IS NULL)
        queryWrapper.isNull("email");
        int result = userMapper.delete(queryWrapper);
        System.out.println("result:"+result);

    }

    @Test
    public void test04(){
        //修改年龄大于20,并且用户名包含a,或者邮箱为null的用户信息
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        //UPDATE t_user SET user_name=?, email=? WHERE is_deleted=0 AND (age > ? AND user_name LIKE ? OR email IS NOT NULL)
        queryWrapper.gt("age",20)
                .like("user_name","a")
                .or()
                .isNotNull("email");
        User user = new User();
        user.setName("小红");
        user.setEmail("353535@qq.com");
        int result= userMapper.update(user,queryWrapper);
        System.out.println("result:"+result);

    }
    @Test
    void test05(){
        //将用户名包含a并且(年龄大于20或邮箱为null)的用户信息
        //lombda中的条件优先执行
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        //UPDATE t_user SET user_name=? WHERE is_deleted=0 AND (user_name LIKE ? AND (age > ? OR email IS NULL))
        queryWrapper.like("user_name","a")
                .and(i->i.gt("age",20).or().isNull("email"));
        User user = new User();
        user.setName("小明");
        int result = userMapper.update(user, queryWrapper);
        System.out.println("result:"+result);

    }
    @Test
    public void test06(){
        //查询用户的用户名,年龄,邮箱信息
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        //SELECT user_name,age,email FROM t_user WHERE is_deleted=0
        queryWrapper.select("user_name","age","email");

        List<Map<String, Object>> maps = userMapper.selectMaps(queryWrapper);
        maps.forEach(System.out::println);
    }

    @Test
    public void test07(){
        //查询id小于等于100的用户信息
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        //SELECT uid AS id,user_name AS name,age,email,is_deleted FROM t_user WHERE is_deleted=0 AND (uid IN (select uid from t_user where uid <=100))
        queryWrapper.inSql("uid","select uid from t_user where uid <=100");
        List<User> list = userMapper.selectList(queryWrapper);
        list.forEach(System.out::println);
    }

    @Test
    public void test08(){
        //将用户名包含有a的......
        UpdateWrapper<User> userUpdateWrapper = new UpdateWrapper<>();
        //UPDATE t_user SET user_name=?,user_name=?,email=? WHERE is_deleted=0 AND ((age > ? OR email IS NULL))
        userUpdateWrapper.set("user_name","a")
                .and(i->i.gt("age","15").or().isNull("email"));
        userUpdateWrapper.set("user_name","胡歌").set("email","maxiangyu@qq.com");
        int result = userMapper.update(null,userUpdateWrapper);
        System.out.println("result:"+result);
    }

    @Test
    public void test09(){
        String userName = "";
        Integer ageBegin = 10;
        Integer ageEnd = 50;

        //SELECT uid AS id,user_name AS name,age,email,is_deleted FROM t_user WHERE is_deleted=0 AND (age >= ? AND age <= ?)
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        //isNotNull:判断某个字段是否不为空,"",或者空白符
        if (StringUtils.isNotBlank(userName)){
            queryWrapper.like("user_name",userName);
        }
        if (ageBegin != null){
            queryWrapper.ge("age",ageBegin);
        }
        if (ageEnd !=null){
            queryWrapper.le("age",ageEnd);
        }

        List<User> list = userMapper.selectList(queryWrapper);
        list.forEach(System.out::println);
    }

    @Test
    public void test10(){
        String userName = "a";
        Integer ageBegin = null;
        Integer ageEnd = 50;

        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        //SELECT uid AS id,user_name AS name,age,email,is_deleted FROM t_user WHERE is_deleted=0 AND (user_name LIKE ? AND age <= ?)
        queryWrapper.like(StringUtils.isNotBlank(userName),"user_name",userName)
                .ge(ageBegin != null,"age",ageBegin)
                .le(ageEnd !=null,"age",ageEnd);
        List<User> list = userMapper.selectList(queryWrapper);
        list.forEach(System.out::println);
    }

    @Test
    public void test11(){
        String userName = "a";
        Integer ageBegin = null;
        Integer ageEnd = 50;

        LambdaQueryWrapper<User> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        //相对于queryWrapper条件封装较为保险,避免了字段写错的问题
        //SELECT uid AS id,user_name AS name,age,email,is_deleted FROM t_user WHERE is_deleted=0 AND (user_name LIKE ? AND age <= ?)
        lambdaQueryWrapper.like(StringUtils.isNotBlank(userName),User::getName,userName)
                .ge(ageBegin !=null,User::getAge,ageBegin)
                .le(ageEnd != null,User::getAge,ageEnd);
        List<User> list = userMapper.selectList(lambdaQueryWrapper);
        list.forEach(System.out::println);

    }

    @Test
    public void test12(){
        //将用户名包含 a 并且(年龄大于20或者邮箱为null的)用户信息修改

        LambdaUpdateWrapper<User> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        //UPDATE t_user SET user_name=?,age=? WHERE is_deleted=0 AND (user_name LIKE ? AND (age >= ? OR email IS NULL))
        lambdaUpdateWrapper.like(User::getName,"a")
                .and(i->i.ge(User::getAge,15).or().isNull(User::getEmail));
        lambdaUpdateWrapper.set(User::getName,"成龙")
                .set(User::getAge,67);
        int result = userMapper.update(null, lambdaUpdateWrapper);
        System.out.println("result:"+result);

    }


}
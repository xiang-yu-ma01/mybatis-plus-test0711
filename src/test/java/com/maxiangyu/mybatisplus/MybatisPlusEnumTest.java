package com.maxiangyu.mybatisplus;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.maxiangyu.mybatisplus.enums.SexEnum;
import com.maxiangyu.mybatisplus.mapper.ProductMapper;
import com.maxiangyu.mybatisplus.mapper.UserMapper;
import com.maxiangyu.mybatisplus.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @ClassName: MybatisPlusEnumTest
 * @Description: 枚举测试类
 * @Author: maxiangyu
 * @Date: 2022/07/15 17:10
 * @Version 1.0
 */
@SpringBootTest
public class MybatisPlusEnumTest {
    @Autowired
    private UserMapper userMapper;

    @Test
    public void test(){
        User user = new User();
        user.setName("admin");
        user.setAge(20);
        user.setSex(SexEnum.MALE);
        int result = userMapper.insert(user);
        System.out.println("result:"+result);
    }
}
package com.maxiangyu.mybatisplus.mybatisservice.mybatisimpl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.maxiangyu.mybatisplus.mapper.UserMapper;
import com.maxiangyu.mybatisplus.mybatisservice.UserService;
import com.maxiangyu.mybatisplus.pojo.User;
import org.springframework.stereotype.Service;

/**
 * @ClassName: UserServiceImpl
 * @Description: IService实现类
 * @Author: maxiangyu
 * @Date: 2022/07/13 09:44
 * @Version 1.0
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
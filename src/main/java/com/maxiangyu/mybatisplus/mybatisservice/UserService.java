package com.maxiangyu.mybatisplus.mybatisservice;

import com.baomidou.mybatisplus.extension.service.IService;
import com.maxiangyu.mybatisplus.pojo.User;

/**
 * @InterfaceName: UserService
 * @Description: 测试mybatispulus的serviece接口
 * @Author: maxiangyu
 * @Date: 2022/07/13 09:42
 * @Version 1.0
 */
public interface UserService extends IService<User> {

}
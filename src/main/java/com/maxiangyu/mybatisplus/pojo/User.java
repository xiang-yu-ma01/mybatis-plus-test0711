package com.maxiangyu.mybatisplus.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.maxiangyu.mybatisplus.enums.SexEnum;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @ClassName: User
 * @Description: User
 * @Author: maxiangyu
 * @Date: 2022/07/11 22:28
 * @Version 1.0
 */
@Data
//设置实体类对应的表名
@TableName("t_user")
public class User {
    //@TableId注解,将属性对应字段作为id主键
    //@TableId注解的value属性用于指定主键的字段
//    @TableId(value = "uid"),当只有一个的时候value可以省略
    //@TableId注解的type属性设置主建生成策略,但是数据库中的主键必须已经设置自增
    @TableId(value="uid",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "user_name")
    private String name;

    private Integer age;

    private String email;


    private SexEnum sex;

    //逻辑删除注解
    @TableLogic
    private Integer isDeleted;

}
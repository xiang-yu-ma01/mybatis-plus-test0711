package com.maxiangyu.mybatisplus.pojo;

import com.baomidou.mybatisplus.annotation.Version;
import com.maxiangyu.mybatisplus.enums.SexEnum;
import lombok.Data;

/**
 * @ClassName: Product
 * @Description: Product实体类
 * @Author: maxiangyu
 * @Date: 2022/07/15 16:20
 * @Version 1.0
 */
@Data
public class Product {

    private Long id;

    private String name;

    private Integer price;


    @Version//标识乐观锁版本号字段
    private Integer version;



}
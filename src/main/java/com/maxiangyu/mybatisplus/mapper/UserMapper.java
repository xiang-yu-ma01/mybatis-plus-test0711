package com.maxiangyu.mybatisplus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.maxiangyu.mybatisplus.pojo.User;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


import java.util.Map;

/**
 * @InterfaceName: UserMapper
 * @Description: Usermapper
 * @Author: maxiangyu
 * @Date: 2022/07/11 22:35
 * @Version 1.0
 */

@Mapper
public interface UserMapper extends BaseMapper<User> {
    //不知道为什么报红,但是目前没有影响
    //在mapper的xml配置文件中,sql的字段与表名要和表中实际相同
    @MapKey("id")//加上这个注解就不报红的,虽然现在不知道为什么
    Map<String,Object> selectMapById(int id);

    /**
     * 通过年龄查询用户信息分页
     * @param page mybatis-plus所提供的分页对象,必须位于第一个参数的位置
     * @param age
     * @return
     */
    Page<User> selectPageVo(@Param("page") Page<User> page,@Param("age") Integer age);

}
package com.maxiangyu.mybatisplus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.maxiangyu.mybatisplus.pojo.Product;
import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.annotation.MapperScan;

/**
 * @InterfaceName: ProductMapper
 * @Description: 接口
 * @Author: maxiangyu
 * @Date: 2022/07/15 16:22
 * @Version 1.0
 */
@Mapper
public interface ProductMapper extends BaseMapper<Product> {

}